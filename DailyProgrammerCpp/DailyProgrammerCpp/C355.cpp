#include "C355.h"

using namespace std;

C355::C355() {
}


C355::~C355() {
}

string C355::cipher(string secret, string message) {
	string str = message;
	int i = 0;
	while (i<message.length()) {
		str[i] = ((message[i] - 'a' + secret[i%secret.length()] - 'a') % 26) + 'a';
		i++;
	}
	
	return str;
}

std::string C355::decipher(std::string secret, std::string crypt) {
	string str = crypt;
	int i = 0;
	while (i < crypt.length()) {
		str[i] = (((crypt[i] - 'a') - (secret[i%secret.length()] - 'a') + 26) % 26) + 'a';
		i++;
	}

	return str;
}
 
int* C355::solve(int one, int two, int three, int four, int five) {
	int *list = new int[2];
	int numberOfPumpkinPies = 0;
	int maxPie = 0;
	while (numberOfPumpkinPies <= one && numberOfPumpkinPies * 3 <= three && numberOfPumpkinPies * 4 <= four && numberOfPumpkinPies * 3 <= five) {
		if (numberOfPumpkinPies + maxApplePies(two, three - (numberOfPumpkinPies * 3) , four - (numberOfPumpkinPies * 4) , five - (numberOfPumpkinPies * 3)) > maxPie+ maxApplePies(two, three - (maxPie * 3), four - (maxPie * 4), five - (maxPie * 3))) {
			maxPie = numberOfPumpkinPies;
		}
		numberOfPumpkinPies++;
	}
	list[0] = maxPie;
	list[1] = maxApplePies(two, three - (maxPie * 3), four - (maxPie * 4), five - (maxPie * 3));
	return list;
}

int C355::maxApplePies(int two, int three, int four, int five) {
	if (two<=0||three<=0||four<=0||five<=0) {
		return 0;
	}
	int max = two;
	if (three / 4 < max) {
		max = three / 4;
	}
	if (four / 3 < max) {
		max = four / 3;
	}
	if (five / 2 < max) {
		max = five / 2;
	}
	return max;
}

