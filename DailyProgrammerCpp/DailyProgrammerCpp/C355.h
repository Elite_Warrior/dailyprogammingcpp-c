#pragma once
#include <iostream>
#include <string>

class C355 {
private:
	int maxApplePies(int two, int three, int four, int five);

public:
	C355();
	~C355();
	//[Easy] Alphabet Cipher
	static std::string cipher(std::string secret, std::string message);
	static std::string decipher(std::string secret, std::string crypt);
	//[Intermediate] Possible Number of Pies
	int* solve(int one, int two, int three, int four, int five);
};

