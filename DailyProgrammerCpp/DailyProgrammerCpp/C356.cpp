#include "C356.h"


C356::C356() {
}


C356::~C356() {

}

bool C356::isPrime(int n) {
	if (n < 2) {
		return false;
	} else if( n == 2) {
		return true;
	} else {
		int count = 3;
		while (count < n) {
			if (n % count == 0) {
				return false;
			}
			count++;
		}
		return true;
	}
}

int C356::nextPrime(int n) {
	n++;
	while (!isPrime(n)) {
		n++;
	}
	return n;
}

void C356::goldbach(int n) {
	int n1 = n-4, n2 = 2, n3 = 2, tmp = 0;
	while ((n1 + n2 + n3) != n || !isPrime(n1)) {
		while (!isPrime(n1)) {
			n1--;
		}
		while (n1 + n2 + n3 < n) {
			tmp = n2;
			n2 = nextPrime(n2);
		}
		n2 = tmp;
		while (n1 + n2 + n3 <= n) {
			tmp = n3;
			n3 = nextPrime(n3);
		}
		n3 = tmp;
		if (n1 + n2 + n3 != n) {
			n1--;
		}
	}
	std::cout << n << " = " << n1 << " + " << n2 << " + " << n3 << std::endl;
}

