#pragma once
#include <cmath>
#include <iostream>

class C356 {
private:
	bool isPrime(int n);
	int nextPrime(int n);
public:
	C356();
	~C356();
	void goldbach(int n);
};

